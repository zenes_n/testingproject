package app;

public class InvalidOutsidePeriodTableException extends Exception {
  public InvalidOutsidePeriodTableException() {
    super("Invalid outside period table. Should be unidimensional with 24 cells.");
  }
}
