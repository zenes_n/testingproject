package app;

public class InvalidWeatherException extends Exception {
  public InvalidWeatherException() {
    super("Invalid weather");
  }
}
