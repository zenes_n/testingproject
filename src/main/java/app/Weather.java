package app;

import java.util.Date;

public class Weather {
  private Date date;
  private double windSpeed;
  private int temperature;
  private int humidityPercentage;
  private double pressure;
  private byte[] chanceOfRain = new byte[24];
  private int visibility;

  /*
   * Constructors
   * */

  public Weather() {
  }

  public Weather(Date date, Double windSpeed, int temperature,
                 int humidityPercentage, double pressure, byte[] chanceOfRain,
                 int visibility) {
    this.date = date;
    this.windSpeed = windSpeed;
    this.temperature = temperature;
    this.humidityPercentage = humidityPercentage;
    this.pressure = pressure;
    setChanceOfRain(chanceOfRain);
    this.visibility = visibility;
  }

  public Weather(Weather weather){
    this.setDate(weather.getDate());
    this.setWindSpeed(weather.getWindSpeed());
    this.setTemperature(weather.getTemperature());
    this.setHumidityPercentage(weather.getHumidityPercentage());
    this.setPressure(weather.getPressure());
    this.setChanceOfRain(weather.getChanceOfRain());
    this.setVisibility(weather.getVisibility());
  }

  /*
   * Class methods
   * */

  public static boolean willUmbrellaBeNeeded(boolean[] whenDoIGoOutside, Weather weather, float luckCoeff)
          throws InvalidOutsidePeriodTableException, InvalidWeatherException {

    if (!isOutsidePeriodTableValid(whenDoIGoOutside)) {
      throw new InvalidOutsidePeriodTableException();
    }

    if (!weather.isValid()) {
      throw new InvalidWeatherException();
    }

    if (luckCoeff < 0.4f) {
      return true;
    }

    for (int i = 0; i < whenDoIGoOutside.length; i++) { // check periods for chance of rain
      if (whenDoIGoOutside[i] == true && weather.getChanceOfRain()[i] > 40) {
        return true;
      }
    }

    return false;
  }

  private static boolean isOutsidePeriodTableValid(boolean[] outsidePeriodTable) {
    return (outsidePeriodTable.length == 24);
  }

  public static boolean willUmbrellaBeUseful(boolean[] whenDoIGoOutside, Weather weather, float luckCoeff)
          throws InvalidOutsidePeriodTableException, InvalidWeatherException {
    if (willUmbrellaBeNeeded(whenDoIGoOutside, weather, luckCoeff) == false) {
      return false;
    }

    if (weather.getTemperature() <= 0) {
      return false;
    }

    if (weather.getWindSpeed() > 20) {
      return false;
    }
    return true;
  }

  public static int dayRatingPercentage(Weather weather, boolean likesRain,
                                        boolean likesWind, boolean likesWarmth,
                                        boolean likesCold, float luckCoeff)
          throws InvalidWeatherException {
    if (!weather.isValid()) {
      throw new InvalidWeatherException();
    }

    int dayRating = 100;

    if (Float.compare(luckCoeff, 0.4f) < 0) {
      dayRating = Math.floorDiv(dayRating, 2);
    }

    if (likesWind != weather.isWindy()) {
      dayRating = (int) Math.floor(dayRating / 1.3);
    }

    if (likesCold != weather.isCold()) {
      dayRating = (int) Math.floor(dayRating / 1.3);
    }

    if ((weather.avgChanceOfRain() > 30) != likesRain) {
      dayRating = (int) Math.floor(dayRating / 1.3);
    }

    if (weather.isWarm() != likesWarmth) {
      dayRating = (int) Math.floor(dayRating / 1.3);
    }


    return dayRating;
  }

  public boolean isWarm() {
    if (temperature > 18) {
      return true;
    }
    return false;
  }

  public boolean isCold() {
    if (temperature < 7) {
      return true;
    }
    return false;
  }

  public boolean isWindy() {
    if (windSpeed > 20) {
      return true;
    }
    return false;
  }

  public int avgChanceOfRain() {
    int sum = 0;
    for (byte chanceOfRainAtHour : chanceOfRain) {
      sum += chanceOfRainAtHour;
    }

    sum = (int) (Math.floor(sum / 24));
    return sum;
  }

  public boolean isValid() {
    if (humidityPercentage < 0 || humidityPercentage > 100) {
      return false;
    }

    if (windSpeed < 0) {
      return false;
    }

    if (pressure < 0) {
      return false;
    }

    for (byte cell : chanceOfRain) {
      if (cell < 0 || cell > 100) {
        return false;
      }
    }

    if (visibility <= 0) {
      return false;
    }

    return true;
  }

  /*
   * Getters and setters
   * */

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Double getWindSpeed() {
    return windSpeed;
  }

  public void setWindSpeed(Double windSpeed) {
    this.windSpeed = windSpeed;
  }

  public int getTemperature() {
    return temperature;
  }

  public void setTemperature(int temperature) {
    this.temperature = temperature;
  }

  public int getHumidityPercentage() {
    return humidityPercentage;
  }

  public void setHumidityPercentage(int humidityPercentage) {
    this.humidityPercentage = humidityPercentage;
  }

  public double getPressure() {
    return pressure;
  }

  public void setPressure(double pressure) {
    this.pressure = pressure;
  }

  public byte[] getChanceOfRain() {
    return chanceOfRain;
  }

  public void setChanceOfRain(byte[] chanceOfRain) {
    if (chanceOfRain.length == 24) {
      for (int i = 0; i < 24; i++) {
        this.chanceOfRain[i] = chanceOfRain[i];
      }
    }
  }

  public int getVisibility() {
    return visibility;
  }

  public void setVisibility(int visibility) {
    this.visibility = visibility;
  }
}
