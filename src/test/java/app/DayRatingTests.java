package app;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DayRatingTests {

  @Test(expected = InvalidWeatherException.class)
  public void invalidWeather() throws Throwable {
    Weather weather = new Weather(
            (new Date(2018, 04, 02)),
            -1.1,
            20,
            20,
            500,
            (new byte[24]),
            5);

    Weather.dayRatingPercentage(weather,
            false,
            false,
            false,
            false,
            (long) 0.9);
  }

  @Test
  public void badLuckDay() throws Throwable {
    Weather weather = new Weather(
            (new Date(2018, 04, 02)),
            10.1,
            10,
            20,
            500,
            (new byte[24]),
            5);

    int result = Weather.dayRatingPercentage(weather,
            false,
            false,
            false,
            false,
            0.1f);
    Assert.assertEquals(50, result);
  }

  @Test
  public void doesNotLikeWind() throws Throwable {
    Weather weather = new Weather(
            (new Date(2018, 04, 02)),
            30.1,
            20,
            20,
            500,
            (new byte[24]),
            5);

    int result = Weather.dayRatingPercentage(weather,
            false,
            false,
            true,
            false,
            0.7f);

    Assert.assertTrue(weather.isWindy());
    Assert.assertEquals(76, result);
  }

  @Test
  public void doesNotLikeCold() throws Throwable {
    Weather weather = new Weather(
            (new Date(2018, 04, 02)),
            10.1,
            -10,
            20,
            500,
            (new byte[24]),
            5);

    int result = Weather.dayRatingPercentage(weather,
            false,
            false,
            false,
            false,
            0.7f);

    Assert.assertTrue(weather.isCold());
    Assert.assertEquals(76, result);
  }

  @Test
  public void doesNotLikeRain() throws Throwable {
    Weather weather = new Weather(
            (new Date(2018, 04, 02)),
            30.1,
            -10,
            20,
            500,
            (new byte[24]),
            5);

    byte[] chanceOfRain = new byte[24];
    for (int i = 0; i < 24; i++) {
      chanceOfRain[i] = 90;
    }
    weather.setChanceOfRain(chanceOfRain);

    int result = Weather.dayRatingPercentage(weather,
            false,
            true,
            false,
            true,
            0.7f);

    Assert.assertTrue(weather.avgChanceOfRain() > 30);
    Assert.assertEquals(76, result);
  }

  @Test
  public void doesNotLikeWarmth() throws Throwable {
    Weather weather = new Weather(
            (new Date(2018, 04, 02)),
            30.1,
            20,
            20,
            500,
            (new byte[24]),
            5);

    int result = Weather.dayRatingPercentage(weather,
            false,
            true,
            false,
            false,
            0.7f);

    Assert.assertTrue(weather.isWarm());
    Assert.assertEquals(76, result);
  }

  @Test
  public void bestDay() throws Throwable {
    Weather weather = new Weather(
            (new Date(2018, 04, 02)),
            30.1,
            21,
            20,
            500,
            (new byte[24]),
            5);

    int result = Weather.dayRatingPercentage(weather,
            false,
            true,
            true,
            false,
            0.7f);

    Assert.assertEquals(100, result);
  }

  @Test
  public void worstDay() throws Throwable {
    Weather weather = new Weather(
            (new Date(2018, 04, 02)),
            30.1,
            21,
            20,
            500,
            (new byte[24]),
            5);

    int result = Weather.dayRatingPercentage(weather,
            true,
            false,
            false,
            true,
            0.1f);

    Assert.assertEquals(16, result);
  }
}
