package app;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ValidMethodTests.class,
        SimpleMethodTests.class,
        WillUmbrellaBeNeededTests.class,
        WillUmbrellaBeUsefulTests.class,
        DayRatingTests.class
})

public class FeatureTestSuite {
  // the class remains empty,
  // used only as a holder for the above annotations
}