package app;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

public class ValidMethodTests {

  private Weather weather;

  @Before
  public void createWeather() {
    weather = new Weather(
            (new Date(2018, 04, 02)),
            20.4,
            20,
            20,
            500,
            (new byte[24]),
            5);
  }

  @Test
  public void IsValidTrue() {
    Assert.assertTrue("Should be true", weather.isValid());
  }

  @Test
  public void IsValidFalse() {
    
    byte[] chanceOfRain = new byte[24];
    chanceOfRain[0] = -80;
    chanceOfRain[1] = 70;
    chanceOfRain[2] = 60;
    weather.setChanceOfRain(chanceOfRain);
    Assert.assertFalse("Should be false", weather.isValid());
  }

  @Test
  public void NegativeHumidity() {
    
    weather.setHumidityPercentage(-1);
    Assert.assertFalse("Should be false", weather.isValid());
  }

  @Test
  public void TooBigHumidity() {
    
    weather.setHumidityPercentage(101);
    Assert.assertFalse("Should be false", weather.isValid());
  }

  @Test
  public void NegativeWindSpeed() {
    
    weather.setWindSpeed(-1.0);
    Assert.assertFalse("Should be false", weather.isValid());
  }

  @Test
  public void NegativePressure() {
    
    weather.setPressure(-1);
    Assert.assertFalse("Should be false", weather.isValid());
  }

  @Test
  public void NegativeVisibility() {
    
    weather.setVisibility(-1);
    Assert.assertFalse("Should be false", weather.isValid());
  }


}
