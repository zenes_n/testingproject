package app;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

public class WillUmbrellaBeNeededTests {
  Weather weather;

  @Before
  public void createWeather() {
    weather = new Weather(
            (new Date(2018, 04, 02)),
            20.4,
            20,
            20,
            500,
            (new byte[24]),
            5);
  }

  @Test(expected = InvalidOutsidePeriodTableException.class)
  public void invalidOutsidePeriod() throws Throwable {
    boolean[] invalidPeriod = new boolean[11];

    Weather.willUmbrellaBeNeeded(invalidPeriod, weather, (long) 0.1);
  }

  @Test(expected = InvalidWeatherException.class)
  public void invalidWeather() throws Throwable {
    boolean[] validPeriod = new boolean[24];
    validPeriod[7] = true;
    validPeriod[8] = true;
    Weather weather = new Weather();
    Weather.willUmbrellaBeNeeded(validPeriod, weather, 1);
  }

  @Test
  public void uselessOutsidePeriod() throws Throwable {


    boolean[] validPeriod = new boolean[24];
    validPeriod[7] = true;
    validPeriod[8] = true;

    Assert.assertFalse(Weather.willUmbrellaBeNeeded(validPeriod, weather, 1f));
  }

  @Test
  public void badLuck() throws Throwable {

    byte[] chanceOfRain = new byte[24];
    chanceOfRain[7] = 20;
    chanceOfRain[8] = 20;

    boolean[] validPeriod = new boolean[24];
    validPeriod[7] = true;
    validPeriod[8] = true;

    Assert.assertTrue(Weather.willUmbrellaBeNeeded(validPeriod, weather, 0.3f));
  }

  @Test
  public void positiveTestCase() throws Throwable {

    byte[] chanceOfRain = new byte[24];
    chanceOfRain[7] = 80;
    chanceOfRain[8] = 80;

    weather.setChanceOfRain(chanceOfRain);

    boolean[] validPeriod = new boolean[24];
    validPeriod[7] = true;
    validPeriod[8] = true;

    Assert.assertTrue(Weather.willUmbrellaBeNeeded(validPeriod, weather, 0.9f));
  }

}
