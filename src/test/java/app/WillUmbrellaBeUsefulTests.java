package app;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

public class WillUmbrellaBeUsefulTests {

  Weather weather;

  @Before
  public void createWeather() {
    weather = new Weather(
            (new Date(2018, 04, 02)),
            20.4,
            20,
            20,
            500,
            (new byte[24]),
            5);
  }

  @Test
  public void umbrellaNotNeeded() throws Throwable {

    byte[] chanceOfRain = new byte[24];
    chanceOfRain[7] = 20;
    chanceOfRain[8] = 20;

    weather.setChanceOfRain(chanceOfRain);

    boolean[] validPeriod = new boolean[24];
    validPeriod[7] = true;
    validPeriod[8] = true;

    Assert.assertFalse(Weather.willUmbrellaBeNeeded(validPeriod, weather, 0.9f));
    Assert.assertFalse(Weather.willUmbrellaBeUseful(validPeriod, weather, 0.9f));
  }

  @Test
  public void negativeTemperature() throws Throwable {

    byte[] chanceOfRain = new byte[24];
    chanceOfRain[7] = 20;
    chanceOfRain[8] = 20;

    weather.setChanceOfRain(chanceOfRain);

    boolean[] validPeriod = new boolean[24];
    validPeriod[7] = true;
    validPeriod[8] = true;

    weather.setTemperature(-1);

    Assert.assertTrue(Weather.willUmbrellaBeNeeded(validPeriod, weather, 0.1f));
    Assert.assertFalse(Weather.willUmbrellaBeUseful(validPeriod, weather, 0.1f));
  }

  @Test
  public void tooWindy() throws Throwable {

    byte[] chanceOfRain = new byte[24];
    chanceOfRain[7] = 80;
    chanceOfRain[8] = 80;

    weather.setChanceOfRain(chanceOfRain);
    weather.setWindSpeed((double) 21);

    boolean[] validPeriod = new boolean[24];
    validPeriod[7] = true;
    validPeriod[8] = true;

    Assert.assertFalse("Should be false", Weather.willUmbrellaBeUseful(validPeriod, weather, 0.9f));
  }

  @Test
  public void positiveTestcase() throws Throwable {

    byte[] chanceOfRain = new byte[24];
    chanceOfRain[7] = 80;
    chanceOfRain[8] = 20;
    chanceOfRain[9] = 80;

    weather.setChanceOfRain(chanceOfRain);
    weather.setWindSpeed(5.0);

    boolean[] validPeriod = new boolean[24];
    validPeriod[7] = false;
    validPeriod[8] = true;
    validPeriod[9] = true;

    Assert.assertTrue("Should be true", Weather.willUmbrellaBeUseful(validPeriod, weather, 0.9f));
  }

}
