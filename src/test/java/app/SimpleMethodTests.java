package app;

import org.junit.Assert;
import org.junit.Test;

public class SimpleMethodTests {
  @Test
  public void IsWarmMethodTrue() {
    Weather weather = new Weather();
    weather.setTemperature(19);
    Assert.assertTrue("Failure - should be true", weather.isWarm());
  }

  @Test
  public void IsWarmMethodFalse() {
    Weather weather = new Weather();
    weather.setTemperature(18);
    Assert.assertFalse("Failure - should be false", weather.isWarm());
  }

  @Test
  public void IsColdMethodTrue() {
    Weather weather = new Weather();
    weather.setTemperature(6);
    Assert.assertTrue("Failure - should be true", weather.isCold());
  }

  @Test
  public void IsColdMethodFalse() {
    Weather weather = new Weather();
    weather.setTemperature(7);
    Assert.assertFalse("Failure - should be false", weather.isCold());
  }

  @Test
  public void IsWindyMethodTrue() {
    Weather weather = new Weather();
    weather.setWindSpeed((double) 21);
    Assert.assertTrue("Failure - should be true", weather.isWindy());
  }

  @Test
  public void IsWindyMethodFalse() {
    Weather weather = new Weather();
    weather.setWindSpeed((double) 20);
    Assert.assertFalse("Failure - should be false", weather.isWindy());
  }

  @Test
  public void AvgChanceOfRain() {
    byte[] chanceOfRain = new byte[24];
    chanceOfRain[0] = 80;
    chanceOfRain[1] = 70;
    chanceOfRain[2] = 60;

    Weather weather = new Weather();
    weather.setChanceOfRain(chanceOfRain);

    Assert.assertEquals(8, weather.avgChanceOfRain());
  }

  @Test
  public void AvgChanceOfRain2() {
    byte[] chanceOfRain = new byte[24];
    chanceOfRain[0] = 80;
    chanceOfRain[1] = 70;
    chanceOfRain[2] = 20;
    chanceOfRain[3] = 50;
    chanceOfRain[4] = 50;
    chanceOfRain[5] = 60;
    chanceOfRain[6] = 60;
    chanceOfRain[7] = 60;

    Weather weather = new Weather();
    weather.setChanceOfRain(chanceOfRain);

    Assert.assertEquals(18, weather.avgChanceOfRain());
  }
}
